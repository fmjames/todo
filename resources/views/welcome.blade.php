@extends('layouts.app')
@section('title','Create Todo')

@section('content')
    <div class="wrapper">
        <a href="{{route('index')}}">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            TODOS LIST
        </a><br>
    </div>




    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Create New Todo</h2>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="list-group">
                                @foreach ($errors->all() as $error)
                                    <li class="list-group-item">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{route('create-todo')}}" method="post" role="form" id="todo">
                        @csrf
                        <div class="card-body">
                            <div class="form-group card-body">
                                <label for="t_name">Enter Todo Name Here</label>
                                <input type="text" name="todo_name" placeholder="Todo Name" id="t_name" class="form-control">
                            </div>
                            <div class="form-group card-body">
                                <label for="t_desc">Enter Todo Description Here</label>
                                <input type="text" name="todo_desc" placeholder="Todo Description" id="t_desc" class="form-control">
                            </div>
                        </div>
                    </form>

                    <div class="card-footer" align="center">

                        <button class="btn btn-success" form="todo">Submit</button>

                    </div>


                </div>
            </div>

        </div>

    </div>
    @endsection
