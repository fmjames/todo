@extends('layouts.app')
@section('title','Edit TODO')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Update your Todo</h2>
                    </div>

{{--                    @if ($errors->any())--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            <ul class="list-group">--}}
{{--                                @foreach ($errors->all() as $error)--}}
{{--                                    <li class="list-group-item">{{ $error }}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endif--}}

                    <form action="/todo/{{$todo->id}}/update" method="post" role="form" id="todo">
                        @csrf
                        <div class="card-body">
                            <div class="form-group card-body">
                                <label for="t_name">Enter Todo Name Here</label>
                                <input type="text" name="todo_name" placeholder="Todo Name" id="t_name" class="form-control" value="{{$todo->name}}">
                            </div>
                            <div class="form-group card-body">
                                <label for="t_desc">Enter Todo Description Here</label>
                                <input type="text" name="todo_desc" placeholder="Todo Description" id="t_desc" class="form-control" value="{{$todo->description}}">
                            </div>
                        </div>
                    </form>

                    <div class="card-footer" align="center">

                        <button class="btn btn-success" form="todo">Update</button>

                    </div>


                </div>
            </div>

        </div>

    </div>

    @endsection
