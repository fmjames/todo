@extends('layouts.app')
@section('title','TODO Lists')


@section('content')
    <div class="wrapper">
        <a href="{{route('home')}}">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Home
        </a><br>
    </div>



    {{--<table>--}}
    {{--    <tr>--}}
    {{--        <th>Id</th>--}}
    {{--        <th>Name</th>--}}
    {{--        <th>Description</th>--}}
    {{--    </tr>--}}
    {{--    @foreach($all_data as $key=>$single_data)--}}
    {{--        <tr>--}}
    {{--            <th>{{$key+1}}</th>--}}
    {{--            <th>{{$single_data->name}}</th>--}}
    {{--            <th>{{$single_data->description}}</th>--}}
    {{--        </tr>--}}
    {{--    @endforeach--}}
    {{--</table>--}}
    {{--{{$all_data->links()}}--}}{{--for using paginating--}}
    {{--@php--}}
    {{--    $sl=1;--}}
    {{--@endphp--}}


        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">TODOS</h2>
                    </div>
                    <div class="card-body">
                        @foreach($todos as $todo)
                            <ul class="list-group">

                                <li class="list-group-item">
                                    {{$todo->name}}

                                    <a href="/todo/{{$todo->id}}" class="btn-sm float-right">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        view
                                    </a>
                                </li>

                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>



    @endsection
