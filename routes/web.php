<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');


Route::get('/todo','todosController@index')->name('index');
Route::post('/todo-create','todosController@create')->name('create-todo');
Route::get('/todo-description','todosController@view')->name('view-todo');
Route::get('/todo/{todo}','todosController@show')->name('show-todo');

Route::get('/todo/{todo}/edit','todosController@edit')->name('edit-todo');
Route::post('/todo/{todo}/update','todosController@update')->name('update-todo');

Route::get('/todo/{todo}/delete','todosController@delete')->name('delete-todo');
