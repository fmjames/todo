<?php

namespace App\Http\Controllers;

use App\Models\Todos;
use Illuminate\Http\Request;

class todosController extends Controller
{
    //
    public function index()
    {
//        $all_data=Todos::all();
//        dd($all_data);
//        return view('todos.index',compact('all_data'));
        return view('todos.index')->with('todos', Todos::all());
    }

    public function create(Request $request)
    {
//        $this->validate(request(),[
//            'name'=> 'required|min:6|max:12',
//            'description'=>'required'
//        ]);

//        dd($request->all());
        $request->validate([
            'todo_name' => 'required',
            'todo_desc' => 'required',
        ]);


        Todos::create([
            'name' => $request->todo_name,
            'description' => $request->todo_desc
        ]);
        return redirect()->back();
    }

    public function view()
    {

        return view('todos.description');
    }

    public function show($todoId)
    {

        return view('todos.description')->with('todo', Todos::find($todoId));

    }

    public function edit($todoId)
    {
        return view('todos.edit')->with('todo', Todos::find($todoId));
    }

    public function update($todoId)
    {
        $data = request()->all();
        $todo = Todos::find($todoId);
        $todo->name = $data['todo_name'];
        $todo->description = $data['todo_desc'];
        $todo->save();
        return redirect('/todo');
    }

    public function delete($todoId)
    {
        $todo = Todos::find($todoId);
        $todo->delete();
        return redirect('/todo');
    }
}
